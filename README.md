# GW150914 Analysis Tutorial

A notebook tutorial on analysing the GW150914 gravitational wave signal of two coalescing black holes. This tutorial is aimed at Year 12 or Physics undergraduates with basic coding and some physics knowlegde.

This tutorial was adapted from a tutorial for Relativity and Cosmology students provided by Monash University, Monash Centre for Astrophysics, School of Physics and Astronomy, 2017.
